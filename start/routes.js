'use strict'


/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome')

//Fetches all interns
Route.get('/interns', 'InternController.index')

//Fetch single intern
Route.get('/interns/:id', 'InternController.interns')

//Create new intern
Route.post('/interns', 'InternController.store')

//Updates intern
Route.put('/interns/:id', 'InternController.update')

//Delete intern
Route.delete('interns/:id', 'InternController.destroy')


