'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InternSchema extends Schema {
  up () {
    this.createIfNotExists('interns', (table) => {
      table.increments('id')
      table.string('name', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('stacks')
      table.timestamps()
    })
  }

  down () {
    this.drop('interns')
  }
}

module.exports = InternSchema
