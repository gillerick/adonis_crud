'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StackSchema extends Schema {
  up () {
    this.create('stacks', (table) => {
      table.increments('id')
      table.string('name')
    })
  }

  down () {
    this.drop('stacks')
  }
}

module.exports = StackSchema
