# Adonis Database CRUD

A simple Adonis project that connects to MySQL database using Lucid ORM


## Setup

Cloning the application

```git
git clone https://gitlab.com/gillerick/adonis_crud.git
```

Starting the application

```bash
adonis serve --dev
```

The application will be started [here](http://127.0.0.1:3333)


### Demo
Fetch All Interns

![Fetch All Interns](assets/demos/all_interns.png)

Fetch Intern

![Fetch Intern](assets/demos/single_intern.png)

Register Intern

![Register Intern](assets/demos/register_intern.png)

Update Intern

![Update Intern](assets/demos/update_intern.png)

Delete Intern

![Delete Intern](assets/demos/delete_intern.png)

### API Documentation
The API documentation can be viewed [here](https://gillerick.github.io/adonis_database_crud_api_documentation/)