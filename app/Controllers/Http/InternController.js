'use strict'

const Intern = use('App/Models/Intern')

const {validate} = use('Validator')

class InternController {
    
    async index(){
        return Intern.all()

    }

    async interns({response, params:{id}}){
        if (Intern.find(id) == null){
            return response.status(204).json("User Not Found")
        }

        response.header('Content-Type', 'application.json')
        return Intern.find(id)
    }

    async store({request, session, response}) {
        const rules = {
            email: 'required|email',
            name: 'required|string',
            stacks: 'required|string'
        }

    const validation = await validate(request.all(), rules)

    if (validation.fails()) {session.withErrors(validation.messages())

      return response.status(403).json({Error: "Wrong Input Format"})
    }

    let userRequest = request.only(['name', 'email', 'stacks'])

    //New instance of Intern class
    const intern = new Intern()

    //Populate intern
    intern.email = userRequest.email
    intern.name = userRequest.name
    intern.stacks = userRequest.stacks

    //Store data in database
    await intern.save()

    response.header('Content-Type', 'application.json')
    return response.json({message: "New User Created",data: intern})
  }

  async update({request, session, response, params:{id}}){
    const rules = {
        email: 'email',
        name: 'string',
        stacks: 'string'
      }
  
      const validation = await validate(request.all(), rules)
  
      if (validation.fails()) {session.withErrors(validation.messages())
  
        return response.status(403).json({Error: "Wrong Input Format"})
      }
    
    let userRequest = request.only(['name', 'email', 'stacks'])

    //New instance of Intern class
    const intern = await Intern.find(id)

    if (!intern){
        return response.status(404).json({Error: "User Not Found"})
    }

    //Populate intern
     if(userRequest.email != null){
        intern.email = userRequest.email
      }else if (userRequest.name != null){
        intern.name = userRequest.name
      }else if(userRequest.stacks != null){
        intern.stacks = userRequest.stacks
      }

    //Update user records
    await intern.save()

    response.header('Content-Type', 'application.json')
        return response.json({
        message: `User successfully updated`,
        data: intern})
    }

  

  //Deletes a specific intern & returns new interns array
    async destroy({response, params: {id}}){
        var intern = await Intern.find(id)
        if (!intern){
            return response.status(404).json({Error:"Record Not Found"})
        }else{
            intern.delete()
        }
    
    response.header('Content-Type', 'application.json')
    return response.json({
      message: `UserID ${id} Deleted`,
      data: intern})
  
    }
}

module.exports = InternController
